package com.viking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

import com.viking.dao.ClientDao;
import com.viking.entity.Client;

public class ClientDaoTest extends JpaTest {

	@Test
	public void testCreateAndDeleteClient() throws Exception {
		ClientDao clientDao = new ClientDao(getEntity());

		Client client = new Client("Doe", "Jane", "123");

		clientDao.create(client);
		clientDao.delete(client);
		Client clientDeleted = getEntity().find(Client.class, client.getId());

		assertNotNull(client.getId());
		assertNull(clientDeleted);
	}

	@Test
	public void testGetAllClient() throws Exception {
		ClientDao clientDao = new ClientDao(getEntity());

		List<Client> clients = clientDao.get();

		clients.forEach(c -> System.out.println(c.getId()));

		assertFalse(clients.isEmpty());
	}
	
	@Test
	public void testGetClient() throws Exception {
		ClientDao clientDao = new ClientDao(getEntity());
		Long id = 1L;
		Client client = clientDao.get(id);

		assertEquals(id, client.getId());
	}


}
