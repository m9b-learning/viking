package com.viking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.viking.dao.ArticleDao;
import com.viking.dao.PanierDao;
import com.viking.entity.Article;
import com.viking.entity.Panier;
import com.viking.service.PanierService;

public class PanierDaoTest extends JpaTest {

	@Test
	public void testCreateAndDeletePanier() throws Exception {
		PanierDao panierDao = new PanierDao(getEntity());
		Panier panier = new Panier();

		panierDao.create(panier);
		panierDao.delete(panier);
		Panier panierDeleted = getEntity().find(Panier.class, panier.getId());

		assertNotNull(panier.getId());
		assertNull(panierDeleted);
	}

	@Test
	public void testGetTotalPanier() throws Exception {
		ArticleDao articleDao = new ArticleDao(getEntity());
		Article article = articleDao.get(1L);

		Panier panier = new Panier();

		panier.addArticle(article);
		panier.addArticle(article);

		double totalAssert = article.getPrix() * 2.0;
		System.out.println(panier.getTotalPanier());

		assertEquals(totalAssert, panier.getTotalPanier(), 0);
	}
}
