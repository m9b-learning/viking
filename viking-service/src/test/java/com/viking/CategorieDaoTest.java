package com.viking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

import com.viking.dao.CategorieDao;
import com.viking.entity.Categorie;

public class CategorieDaoTest extends JpaTest {

	@Test
	public void testCreateAndDeleteCategorie() throws Exception {
		CategorieDao categorieDao = new CategorieDao(getEntity());

		Categorie categorie = new Categorie();
		categorie.setNom("test");

		categorieDao.create(categorie);
		categorieDao.delete(categorie);
		Categorie categorieDeleted = getEntity().find(Categorie.class, categorie.getId());

		assertNotNull(categorie.getId());
		assertNull(categorieDeleted);
	}

	@Test
	public void testGetAllCategorie() throws Exception {
		CategorieDao categorieDao = new CategorieDao(getEntity());

		List<Categorie> categories = categorieDao.get();

		assertFalse(categories.isEmpty());
	}

	@Test
	public void testGetCategorieById() throws Exception {
		CategorieDao categorieDao = new CategorieDao(getEntity());

		Long id = 1L;
		Categorie categorie = categorieDao.get(id);

		assertNotNull(categorie);
		assertEquals(id, categorie.getId());
	}

}
