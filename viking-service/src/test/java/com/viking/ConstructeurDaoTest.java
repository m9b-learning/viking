package com.viking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

import com.viking.dao.ConstructeurDao;
import com.viking.entity.Constructeur;

public class ConstructeurDaoTest extends JpaTest {

	@Test
	public void testCreateAndDeleteConstructeur() throws Exception {
		ConstructeurDao constructeurDao = new ConstructeurDao(getEntity());

		Constructeur constructeur = new Constructeur();
		constructeur.setNom("test");

		constructeurDao.create(constructeur);
		constructeurDao.delete(constructeur);
		Constructeur constructeurDeleted = getEntity().find(Constructeur.class, constructeur.getId());

		assertNotNull(constructeur.getId());
		assertNull(constructeurDeleted);
	}

	@Test
	public void testGetAllConstructeur() throws Exception {
		ConstructeurDao constructeurDao = new ConstructeurDao(getEntity());

		List<Constructeur> constructeurs = constructeurDao.get();

		constructeurs.forEach(c -> System.out.println(c.getNom()));

		assertFalse(constructeurs.isEmpty());
	}

	@Test
	public void testGetConstructeurById() throws Exception {
		ConstructeurDao constructeurDao = new ConstructeurDao(getEntity());

		Long id = 1L;
		Constructeur constructeur = constructeurDao.get(id);

		assertNotNull(constructeur);
		assertEquals(id, constructeur.getId());
	}

}
