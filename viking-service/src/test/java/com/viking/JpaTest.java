package com.viking;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class JpaTest {

	private static EntityManagerFactory emf;
	private EntityManager entity;

	public EntityManager getEntity() {
		return entity;
	}

	@BeforeClass
	public static void createEntityManagerFactory() throws IOException {
		Properties properties = new Properties();
		properties.load(new FileReader("src/main/resources/jdbc.properties"));

		Map<String, String> jpaProperties = new HashMap<>();
		jpaProperties.put("javax.persistence.jdbc.url", properties.getProperty("jdbc.url"));
		jpaProperties.put("javax.persistence.jdbc.driver", properties.getProperty("jdbc.driverClassName"));
		jpaProperties.put("javax.persistence.jdbc.user", properties.getProperty("jdbc.username"));
		jpaProperties.put("javax.persistence.jdbc.password", properties.getProperty("jdbc.password"));

		emf = Persistence.createEntityManagerFactory("viking-project", jpaProperties);
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		if (emf != null) {
			emf.close();
		}
	}

	@Before
	public void createEntityManager() {
		entity = emf.createEntityManager();
	}

	@After
	public void closeEntityManager() {
		entity.close();
	}

}
