package com.viking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

import com.viking.dao.ArticleDao;
import com.viking.dao.CategorieDao;
import com.viking.dao.ConstructeurDao;
import com.viking.entity.Article;
import com.viking.entity.Categorie;
import com.viking.entity.Constructeur;

public class ArticleDaoTest extends JpaTest {

	@Test
	public void testCreateAndDeleteArticle() throws Exception {
		CategorieDao categorieDao = new CategorieDao(getEntity());
		Categorie categorie = categorieDao.get(1L);

		ConstructeurDao constructeurDao = new ConstructeurDao(getEntity());
		Constructeur constructeur = constructeurDao.get(1L);

		ArticleDao articleDao = new ArticleDao(getEntity());
		Article article = new Article("article test", 10.0, "ceci est un article de test", categorie, constructeur, 10);

		articleDao.create(article);
		articleDao.delete(article);
		Article articleDeleted = getEntity().find(Article.class, article.getId());

		assertNotNull(article.getId());
		assertNull(articleDeleted);
	}
	
	@Test
	public void testArticle() throws Exception {
		ArticleDao articleDao = new ArticleDao(getEntity());
		Long id = 1L;
		
		Article article = articleDao.get(id);

		assertEquals(id, article.getId());
	}

	@Test
	public void testGetAllArticle() throws Exception {
		ArticleDao articleDao = new ArticleDao(getEntity());

		List<Article> articles = articleDao.get();

		assertFalse(articles.isEmpty());
	}

	@Test
	public void testGetAllArticleByCategorie() throws Exception {
		ArticleDao articleDao = new ArticleDao(getEntity());
		CategorieDao categorieDao = new CategorieDao(getEntity());
		Categorie categorie = categorieDao.get(1L);

		List<Article> articles = articleDao.get(categorie);

		assertFalse(articles.isEmpty());
	}

	@Test
	public void testGetAllArticleByConstructeur() throws Exception {
		ArticleDao articleDao = new ArticleDao(getEntity());
		ConstructeurDao constructeurDao = new ConstructeurDao(getEntity());
		Constructeur constructeur = constructeurDao.get(1L);

		List<Article> articles = articleDao.get(constructeur);

		assertFalse(articles.isEmpty());
	}

}
