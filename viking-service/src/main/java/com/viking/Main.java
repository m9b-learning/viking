package com.viking;

import java.util.List;

import org.springframework.context.support.GenericXmlApplicationContext;

import com.viking.dao.PanierDao;
import com.viking.entity.Article;
import com.viking.entity.Panier;
import com.viking.service.PanierService;
import com.viking.service.exceptions.PanierNotFoundException;

public class Main {

	public static void main(String[] args) {

//		GenericXmlApplicationContext appCtx = new GenericXmlApplicationContext("classpath:META-INF/viking-context.xml");
//
//		try {
//			PanierDao pdao = appCtx.getBean(PanierDao.class);
//			
//			Panier panier = pdao.get(10L);
//			for (Article article : panier.getArticles()) {
//				System.out.println(article.getDesignation());
//			}
//
//
//		} finally {
//			appCtx.close();
//		}

		PanierService panierService = new PanierService();
		List<Panier> list = panierService.get();
		
		System.out.println(list);
//		try {
//			panierService.validePanier(10L);
//			
//		} catch (PanierServiceException e) {
//			e.printStackTrace();
//		}

		/*
		 * EntityManagerFactory emf =
		 * Persistence.createEntityManagerFactory("viking-project"); EntityManager
		 * entity = emf.createEntityManager();
		 * 
		 * try { Categorie categorie = entity.find(Categorie.class, 1L);
		 * System.out.println(categorie.getNom());
		 * 
		 * entity.getTransaction().begin(); categorie.setNom("écran");
		 * entity.getTransaction().commit();
		 * 
		 * 
		 * } finally { entity.close(); emf.close();
		 * 
		 * }
		 */
	}

}
