package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Categorie;

@Component
public class CategorieDao {

	@PersistenceContext
	private EntityManager entityManager;

	private static final String SELECT_ALL_CATEGORIE = "select i from Categorie i order by i.nom";
	private static final String DELETE_CATEGORIE = "delete from Categorie i where i.id = :id";

	public CategorieDao() {
	}

	public CategorieDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional
	public void create(Categorie categorie) {
		entityManager.persist(categorie);
	}

	@Transactional
	public void delete(Categorie categorie) {
		entityManager.remove(categorie);
	}

	@Transactional
	public boolean delete(Long id) {
		return entityManager.createQuery(DELETE_CATEGORIE).setParameter("id", id).executeUpdate() == 1;
	}

	public List<Categorie> get() {
		return entityManager.createQuery(SELECT_ALL_CATEGORIE, Categorie.class).getResultList();
	}

	public Categorie get(Long id) {
		return entityManager.find(Categorie.class, id);
	}
}
