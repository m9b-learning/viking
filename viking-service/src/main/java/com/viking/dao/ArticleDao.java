package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Article;
import com.viking.entity.Categorie;
import com.viking.entity.Constructeur;

@Component
public class ArticleDao {

	@PersistenceContext
	private EntityManager entityManager;

	private static final String SELECT_ALL_ARTICLE = "select a from Article a";
	private static final String SELECT_ALL_ARTICLE_BY_CATEGORIE = "select a from Article a where a.categorie.id = :id";
	private static final String SELECT_ALL_ARTICLE_BY_CONSTRUCTEUR = "select a from Article a where a.constructeur.id = :id";
	private static final String DELETE_ARTICLE = "delete from Article a where a.id = :id";

	public ArticleDao() {
	}

	public ArticleDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional
	public void create(Article article) {
		entityManager.persist(article);
	}

	@Transactional
	public void delete(Article article) {
		entityManager.remove(article);
	}

	@Transactional
	public boolean delete(Long id) {
		return entityManager.createQuery(DELETE_ARTICLE).setParameter("id", id).executeUpdate() == 1;
	}

	public List<Article> get() {
		return entityManager.createQuery(SELECT_ALL_ARTICLE, Article.class).getResultList();
	}

	public Article get(Long id) {
		return entityManager.find(Article.class, id);
	}

	public List<Article> get(Categorie categorie) {
		return entityManager.createQuery(SELECT_ALL_ARTICLE_BY_CATEGORIE, Article.class)
				.setParameter("id", categorie.getId()).getResultList();
	}

	public List<Article> get(Constructeur constructeur) {
		return entityManager.createQuery(SELECT_ALL_ARTICLE_BY_CONSTRUCTEUR, Article.class)
				.setParameter("id", constructeur.getId()).getResultList();
	}
}
