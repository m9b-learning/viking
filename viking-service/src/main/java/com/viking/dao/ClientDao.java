package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Client;
import com.viking.entity.Panier;

@Component
public class ClientDao {

	@PersistenceContext
	private EntityManager entityManager;

	private static final String SELECT_ALL_CLIENT = "select c from Client c";
	private static final String SELECT_CLIENT_BY_PANIER = "select c from Client c where c.panier.id = :id";
	private static final String DELETE_CLIENT = "delete from Client i where i.id = :id";

	public ClientDao() {
	}

	public ClientDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional
	public void create(Client client) {
		entityManager.persist(client);
	}

	@Transactional
	public void delete(Client client) {
		entityManager.remove(client);
	}

	@Transactional
	public void delete(Long id) {
		entityManager.createQuery(DELETE_CLIENT).setParameter("id", id).executeUpdate();
	}

	public List<Client> get() {
		return entityManager.createQuery(SELECT_ALL_CLIENT, Client.class).getResultList();
	}

	public Client get(Long id) {
		return entityManager.find(Client.class, id);
	}

	public Client get(Panier panier) {
		return entityManager.createQuery(SELECT_CLIENT_BY_PANIER, Client.class)
				.setParameter("id", panier.getId()).getSingleResult();
	}
}
