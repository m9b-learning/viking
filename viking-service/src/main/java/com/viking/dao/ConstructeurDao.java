package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Categorie;
import com.viking.entity.Constructeur;

@Component
public class ConstructeurDao {

	@PersistenceContext
	private EntityManager entityManager;

	private static final String SELECT_ALL_CONSTRUCTEUR = "select i from Constructeur i";
	private static final String SELECT_CONSTRUCTEUR_BY_ID = "select i from Constructeur i where i.id = :id";
	private static final String SELECT_CONSTRUCTEUR_BY_NAME = "select i from Constructeur i where i.nom = :nom";
	private static final String DELETE_CONSTRUCTEUR = "delete from Constructeur i where i.id = :id";

	public ConstructeurDao() {
	}

	public ConstructeurDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional
	public void create(Constructeur constructeur) {
		entityManager.persist(constructeur);
	}

	@Transactional
	public void delete(Constructeur constructeur) {
		entityManager.remove(constructeur);
	}

	@Transactional
	public boolean delete(Long id) {
		return entityManager.createQuery(DELETE_CONSTRUCTEUR).setParameter("id", id).executeUpdate() == 1;
	}

	public List<Constructeur> get() {
		return entityManager.createQuery(SELECT_ALL_CONSTRUCTEUR, Constructeur.class).getResultList();
	}

	public Constructeur get(Long id) {
		return entityManager.find(Constructeur.class, id);
	}
}
