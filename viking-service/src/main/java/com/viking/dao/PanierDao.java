package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Panier;

@Component
public class PanierDao {

	@PersistenceContext
	private EntityManager entityManager;

	private static final String SELECT_ALL_PANIER = "select i from Panier i";
	private static final String DELETE_PANIER = "delete from Panier i where i.id = :id";

	public PanierDao() {
	}

	public PanierDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Transactional
	public void create(Panier panier) {
		entityManager.persist(panier);
	}

	@Transactional
	public void delete(Panier panier) {
		entityManager.remove(panier);
	}

	@Transactional
	public boolean delete(Long id) {
		return entityManager.createQuery(DELETE_PANIER).setParameter("id", id).executeUpdate() == 1;
	}

	public List<Panier> get() {
		return entityManager.createQuery(SELECT_ALL_PANIER, Panier.class).getResultList();
	}

	public Panier get(Long id) {
		return entityManager.find(Panier.class, id);
	}
}
