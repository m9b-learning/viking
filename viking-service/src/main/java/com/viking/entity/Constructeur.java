package com.viking.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "constructeur")
@SequenceGenerator(name = "constructeur_seq", allocationSize = 1)
public class Constructeur {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "constructeur_seq")
	private Long id;
	
	@NotEmpty
	private String nom;

	@JsonIgnore
	@OneToMany(mappedBy = "constructeur")
	private List<Article> articles;

	public Constructeur() {
	}

	public Constructeur(String nom) {
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

}
