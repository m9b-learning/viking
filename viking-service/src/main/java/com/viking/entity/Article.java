package com.viking.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "article")
@SequenceGenerator(name = "article_seq", allocationSize = 1)
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "article_seq")
	private Long id;

	@NotEmpty
	private String designation;

	@Column(name = "image")
	private String image;

	@Min(1)
	private double prix;

	@Column(name = "content")
	@NotEmpty
	private String description;

	@ManyToOne
	@JoinColumn(name = "categorie_id")
	@NotNull
	private Categorie categorie;

	@ManyToOne
	@NotNull
	private Constructeur constructeur;

	@Column(name = "datemiseenligne", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false)
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = Shape.STRING)
	private Calendar miseEnLigne;

	@Min(1)
	private int stock;
	
	@Version
	private int version;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "panierarticle", 
		joinColumns = @JoinColumn(name = "article_id"), 
		inverseJoinColumns = @JoinColumn(name = "panier_id"))
	@JsonIgnore
	private List<Panier> paniers;

	public Article() {
	}

	public Article(String designation, double prix, String description, Categorie categorie, Constructeur constructeur,
			int stock) {
		this.designation = designation;
		this.prix = prix;
		this.description = description;
		this.categorie = categorie;
		this.constructeur = constructeur;
		this.stock = stock;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Constructeur getConstructeur() {
		return constructeur;
	}

	public void setConstructeur(Constructeur constructeur) {
		this.constructeur = constructeur;
	}

	public Calendar getMiseEnLigne() {
		return miseEnLigne;
	}

	public void setMiseEnLigne(Calendar miseEnLigne) {
		this.miseEnLigne = miseEnLigne;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public boolean isAvailable(int quantity) {
		return (this.getStock() - quantity) >= 0;
	}

}
