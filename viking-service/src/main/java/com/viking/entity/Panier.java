package com.viking.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "panier")
@SequenceGenerator(name = "panier_seq", allocationSize = 1)
public class Panier {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "panier_seq")
	private Long id;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinTable(name = "panierarticle", joinColumns = @JoinColumn(name = "panier_id"), inverseJoinColumns = @JoinColumn(name = "article_id"))
	private List<Article> articles;

	public Panier() {
		this.articles = new ArrayList<Article>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public void addArticle(Article article) {
		articles.add(article);
	}

	public void deleteArticle(Article article) {
		articles.remove(article);
	}

	public double getTotalPanier() {
		return this.getArticles().stream().mapToDouble(a -> a.getPrix()).sum();
	}

}
