package com.viking.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "categorie")
@SequenceGenerator(name = "categorie_seq", allocationSize = 1)
public class Categorie {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categorie_seq")
	private Long id;

	@NotEmpty
	private String nom;

	@JsonIgnore
	@OneToMany(mappedBy = "categorie")
	private List<Article> articles;

	public Categorie() {
	}

	public Categorie(String nom) {
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

}
