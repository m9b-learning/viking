package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.CategorieDao;
import com.viking.entity.Categorie;
import com.viking.service.exceptions.CategorieNotFoundException;

@Service
public class CategorieService {

	@Autowired
	CategorieDao categorieDao;

	@Transactional(rollbackFor = CategorieNotFoundException.class)
	public void create(Categorie categorie) {
		categorieDao.create(categorie);
	}

	@Transactional(rollbackFor = CategorieNotFoundException.class)
	public void update(Categorie cat) throws CategorieNotFoundException {
		Categorie categorie = get(cat.getId());
		categorie.setNom(cat.getNom());
	}

	@Transactional(rollbackFor = CategorieNotFoundException.class)
	public void delete(Long id) throws CategorieNotFoundException {
		if (!categorieDao.delete(id)) {
			throw new CategorieNotFoundException(id);
		}
	}

	public List<Categorie> get() {
		return categorieDao.get();
	}

	public Categorie get(Long id) throws CategorieNotFoundException {
		Categorie categorie = categorieDao.get(id);
		if (categorie == null) {
			throw new CategorieNotFoundException(id);
		}
		return categorie;
	}
}
