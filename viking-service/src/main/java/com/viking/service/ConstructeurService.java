package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.ConstructeurDao;
import com.viking.entity.Categorie;
import com.viking.entity.Constructeur;
import com.viking.service.exceptions.CategorieNotFoundException;
import com.viking.service.exceptions.ConstructeurNotFoundException;

@Service
public class ConstructeurService {

	@Autowired
	private ConstructeurDao constructeurDao;

	@Transactional(rollbackFor = ConstructeurNotFoundException.class)
	public void create(Constructeur constructeur) {
		constructeurDao.create(constructeur);
	}

	@Transactional(rollbackFor = ConstructeurNotFoundException.class)
	public void update(Constructeur cons) throws ConstructeurNotFoundException {
		Constructeur constructeur = get(cons.getId());
		constructeur.setNom(cons.getNom());
	}

	@Transactional(rollbackFor = ConstructeurNotFoundException.class)
	public void delete(Long id) throws ConstructeurNotFoundException {
		if (! constructeurDao.delete(id)) {
			throw new ConstructeurNotFoundException(id);
		}
	}

	public List<Constructeur> get() {
		return constructeurDao.get();
	}

	public Constructeur get(Long id) throws ConstructeurNotFoundException {
		Constructeur constructeur = constructeurDao.get(id);
		if (constructeur == null) {
			throw new ConstructeurNotFoundException(id);
		}
		return constructeur;
	}


}
