package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.PanierDao;
import com.viking.entity.Article;
import com.viking.entity.Panier;
import com.viking.service.exceptions.ArticleNotFoundException;
import com.viking.service.exceptions.PanierNotFoundException;

@Service
public class PanierService {
	@Autowired
	PanierDao panierDao;

	@Autowired
	ArticleService articleService;

	public void create(Panier panier) {
		panierDao.create(panier);
	}

	public void delete(Long id) throws PanierNotFoundException {
		if (! panierDao.delete(id)) {
			throw new PanierNotFoundException(id);
		};
	}

	public List<Panier> get() {
		return panierDao.get();
	}

	public Panier get(Long id) throws PanierNotFoundException {
		Panier panier = panierDao.get(id);
		if (panier == null) {
			throw new PanierNotFoundException(id);
		}
		return panier;
	}

	@Transactional(rollbackFor = { PanierNotFoundException.class, ArticleNotFoundException.class })
	public Panier validePanier(Long id) throws PanierNotFoundException, ArticleNotFoundException {

		Panier panier = get(id);

		for (Article article : panier.getArticles()) {
			
			//TODO est-ce que je vérifie que l'article existe ? 

			if (!article.isAvailable(1)) {
				//TODO revoir cette exception
				throw new PanierNotFoundException("L'article " + article.getDesignation() + " n'est plus disponible");
			}
			
			article.setStock(article.getStock() - 1);
		}

		// Arrivé ici, le panier a été payé et le stock modifié. Les lignes sont supprimées
		panier.getArticles().clear();

		return panier;
	}
}
