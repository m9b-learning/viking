package com.viking.service.exceptions;

public class ConstructeurNotFoundException extends VikingServiceException {

	public ConstructeurNotFoundException(Long id) {
		super("Le constructeur " + id + " n'existe pas");
	}

}
