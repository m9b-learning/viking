package com.viking.service.exceptions;

public class PanierNotFoundException extends VikingServiceException {

	public PanierNotFoundException(Long id) {
		super("Le panier " + id + " n'existe pas");
	}
	
	public PanierNotFoundException(String message) {
		super(message);
	}
}
