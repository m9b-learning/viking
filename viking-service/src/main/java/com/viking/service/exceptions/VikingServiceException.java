package com.viking.service.exceptions;

public abstract class VikingServiceException extends Exception {

	public VikingServiceException(String message) {
		super(message);
	}
	
}
