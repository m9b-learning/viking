package com.viking.service.exceptions;

public class ClientNotFoundException extends VikingServiceException {

	public ClientNotFoundException(Long id) {
		super("Le client " + id + " n'existe pas");
	}

}
