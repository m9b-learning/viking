package com.viking.service.exceptions;

public class ArticleNotFoundException extends VikingServiceException {
	
	public ArticleNotFoundException(Long id) {
		super("L'article " + id + " n'existe pas");
	}
}
