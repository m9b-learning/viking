package com.viking.service.exceptions;

public class CategorieNotFoundException extends VikingServiceException {

	public CategorieNotFoundException(Long id) {
		super("La catégorie " + id + " n'existe pas");
	}
}
