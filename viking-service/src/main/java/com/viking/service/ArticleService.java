package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.ArticleDao;
import com.viking.entity.Article;
import com.viking.entity.Categorie;
import com.viking.entity.Constructeur;
import com.viking.service.exceptions.ArticleNotFoundException;
import com.viking.service.exceptions.CategorieNotFoundException;
import com.viking.service.exceptions.ConstructeurNotFoundException;

@Service
public class ArticleService {

	@Autowired
	private ArticleDao articleDao;
	
	@Autowired
	private CategorieService categorieService;

	@Autowired
	private ConstructeurService constructeurService;
	
	@Transactional(rollbackFor = ArticleNotFoundException.class)
	public void create(Article article) {
		articleDao.create(article);
	}
	
	@Transactional(rollbackFor = {ArticleNotFoundException.class, CategorieNotFoundException.class, ConstructeurNotFoundException.class})
	public void update (Article art) throws ArticleNotFoundException, CategorieNotFoundException, ConstructeurNotFoundException {
		Article article = get(art.getId());
		Categorie categorie = categorieService.get(art.getCategorie().getId());
		Constructeur constructeur = constructeurService.get(art.getConstructeur().getId());
		
		article.setDesignation(art.getDesignation());
		article.setImage(art.getImage());
		article.setPrix(art.getPrix());
		article.setDescription(art.getDescription());
		article.setCategorie(categorie);
		article.setConstructeur(constructeur);
		article.setStock(art.getStock());
	}

	@Transactional(rollbackFor = ArticleNotFoundException.class)
	public void delete(Long id) throws ArticleNotFoundException {
		if (!articleDao.delete(id)) {
			throw new ArticleNotFoundException(id);
		}
	}

	public List<Article> get() {
		return articleDao.get();
	}

	public Article get(Long id) throws ArticleNotFoundException {
		Article article = articleDao.get(id);
		if (article == null) {
			throw new ArticleNotFoundException(id);
		}
		return article;
	}
}
