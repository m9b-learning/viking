package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.ClientDao;
import com.viking.dao.PanierDao;
import com.viking.entity.Client;
import com.viking.entity.Panier;
import com.viking.service.exceptions.ClientNotFoundException;
import com.viking.service.exceptions.PanierNotFoundException;

@Service
public class ClientService {
	
	@Autowired
	private ClientDao clientDao;
	
	@Autowired
	private PanierService panierService;

	@Transactional(rollbackFor = ClientNotFoundException.class)
	public void create(Client client) {
		Panier panier = new Panier();
		panierService.create(panier);
		client.setPanier(panier);
		
		clientDao.create(client);
	}
	
	@Transactional(rollbackFor = ClientNotFoundException.class)
	public void update(Client cli) throws ClientNotFoundException {
		Client client = get(cli.getId());
		
		Panier panier = new Panier();
		try {
			 panier = panierService.get(client.getPanier().getId());
		} catch (PanierNotFoundException|NullPointerException e) {
			panierService.create(panier);
		}
		
	    client.setNom(cli.getNom());
	    client.setPrenom(cli.getPrenom());
	    client.setAdresse(cli.getAdresse());
	    client.setCp(cli.getCp());
	    client.setVille(cli.getVille());
	    client.setPays(cli.getPays());
	    client.setEmail(cli.getEmail());
	    client.setDateNaissance(cli.getDateNaissance());
	    client.setMdp(cli.getMdp());
	    client.setPanier(panier);
	}

	@Transactional(rollbackFor = ClientNotFoundException.class)
	public void delete(Long id) throws ClientNotFoundException {
		//clientDao.delete(id);
		Client client = get(id);
		clientDao.delete(client.getId());
		if (client.getPanier().getId() != null) {
			try {
				panierService.delete(client.getPanier().getId());
			} catch (PanierNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Client> get() {
		return clientDao.get();
	}

	public Client get(Long id) throws ClientNotFoundException {
		Client client = clientDao.get(id);
        if (client == null) {
            throw new ClientNotFoundException(id);
        }
        return client;
	}
}
;
