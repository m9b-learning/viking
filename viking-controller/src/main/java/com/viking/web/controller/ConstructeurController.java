package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entity.Constructeur;
import com.viking.service.ConstructeurService;
import com.viking.service.exceptions.ConstructeurNotFoundException;

@RestController
@RequestMapping(path = "/constructeur")
public class ConstructeurController {

	@Autowired
	private ConstructeurService constructeurService;

	@PostMapping()
	public ResponseEntity<Constructeur> create(@RequestBody @Valid Constructeur constructeur,
			BindingResult itemBindingResult, UriComponentsBuilder builder) throws FieldErrorException {
		if (itemBindingResult.hasErrors()) {
			throw new FieldErrorException(itemBindingResult);
		}

		constructeurService.create(constructeur);
		URI uri = builder.pathSegment("constructeur", String.valueOf(constructeur.getId())).build().toUri();
		return ResponseEntity.created(uri).body(constructeur);
	}

	@PutMapping(path = "{id}", produces = "application/json")
	public Constructeur update(@PathVariable Long id, @RequestBody @Valid Constructeur constructeur,
			BindingResult itemBindingResult) throws ConstructeurNotFoundException, FieldErrorException {
		if (itemBindingResult.hasErrors()) {
			throw new FieldErrorException(itemBindingResult);
		}

		constructeur.setId(id);
		constructeurService.update(constructeur);

		return constructeur;
	}

	@DeleteMapping(path = "{id}", produces = "application/json")
	public void delete(@PathVariable Long id) throws ConstructeurNotFoundException {
		constructeurService.delete(id);
	}

	@GetMapping(produces = "application/json")
	public List<Constructeur> get() {
		return constructeurService.get();
	}

	@GetMapping(path = "{id}", produces = "application/json")
	public Constructeur get(@PathVariable Long id) throws ConstructeurNotFoundException {
		return constructeurService.get(id);
	}

}