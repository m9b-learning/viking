package com.viking.web.controller;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class FieldErrorException extends Exception {

	private BindingResult itemBindingResult;

	public FieldErrorException(BindingResult itemBindingResult) {
		this.itemBindingResult = itemBindingResult;
	}

	public Map<String, String> getFieldErrors() {
		return itemBindingResult.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
	}

}
