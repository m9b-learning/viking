package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entity.Article;
import com.viking.service.ArticleService;
import com.viking.service.exceptions.ArticleNotFoundException;
import com.viking.service.exceptions.CategorieNotFoundException;
import com.viking.service.exceptions.ConstructeurNotFoundException;

@RestController
@RequestMapping(path = "/article")
public class ArticleController {

	@Autowired
	private ArticleService articleService;

	@PostMapping()
    public ResponseEntity<Article> create(@RequestBody @Valid Article article, BindingResult itemBindingResult, UriComponentsBuilder builder) throws FieldErrorException {
		if (itemBindingResult.hasErrors()) { 
			throw new FieldErrorException(itemBindingResult);
        }
		
        articleService.create(article);
        URI uri = builder.pathSegment("article", String.valueOf(article.getId())).build().toUri();
        return ResponseEntity.created(uri).body(article);
    }
	
	@PutMapping(path = "{id}", produces = "application/json")
	public Article update(@PathVariable Long id, @RequestBody @Valid Article article, BindingResult itemBindingResult) throws ArticleNotFoundException, CategorieNotFoundException, ConstructeurNotFoundException, FieldErrorException  {
		if (itemBindingResult.hasErrors()) { 
			throw new FieldErrorException(itemBindingResult);
        }
		
		article.setId(id);
		articleService.update(article);

		return article;
	}

	@DeleteMapping(path = "{id}", produces = "application/json")
	public void delete(@PathVariable Long id) throws ArticleNotFoundException {
		articleService.delete(id);
	}

	@GetMapping(produces = "application/json")
	public List<Article> get() {
		return articleService.get();
	}

	@GetMapping(path = "{id}", produces = "application/json")
	public Article get(@PathVariable Long id) throws ArticleNotFoundException {
		return articleService.get(id);
	}

}