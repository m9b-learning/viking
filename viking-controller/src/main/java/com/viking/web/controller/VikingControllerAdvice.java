package com.viking.web.controller;

import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.viking.service.exceptions.VikingServiceException;

@RestControllerAdvice("com.viking.web.controller")
public class VikingControllerAdvice {

	@ExceptionHandler(VikingServiceException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@RequestMapping(produces = "application/json")
	public String hanlder(VikingServiceException e) {
		return e.getMessage();
	}

	@ExceptionHandler(FieldErrorException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public Map<String, String> handleBindingErrors(FieldErrorException ex) {
		return ex.getFieldErrors();
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@RequestMapping(produces = "application/json")
	public String hanlder(ConstraintViolationException e) {
		return e.getCause().getLocalizedMessage()  + " >> " + e.getMessage() ;
	}

}
