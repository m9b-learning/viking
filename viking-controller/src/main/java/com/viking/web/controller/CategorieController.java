package com.viking.web.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entity.Categorie;
import com.viking.service.CategorieService;
import com.viking.service.exceptions.CategorieNotFoundException;

@RestController
@RequestMapping(path = "/categorie")
public class CategorieController {

	@Autowired
	private CategorieService categorieService;

	@PostMapping()
	public ResponseEntity<Categorie> create(@RequestBody @Valid Categorie categorie, BindingResult itemBindingResult, UriComponentsBuilder builder) throws FieldErrorException {
		if (itemBindingResult.hasErrors()) { 
			throw new FieldErrorException(itemBindingResult);
        }
		
		categorieService.create(categorie);
		URI uri = builder.pathSegment("categorie", String.valueOf(categorie.getId())).build().toUri();
		return ResponseEntity.created(uri).body(categorie);
	}

	@PutMapping(path = "{id}", produces = "application/json")
	public Categorie update(@PathVariable Long id, @RequestBody @Valid Categorie categorie, BindingResult itemBindingResult) throws CategorieNotFoundException, FieldErrorException {
		if (itemBindingResult.hasErrors()) { 
			throw new FieldErrorException(itemBindingResult);
        }
		
		categorie.setId(id);
		categorieService.update(categorie);

		return categorie;
	}

	@DeleteMapping(path = "{id}", produces = "application/json")
	public void delete(@PathVariable Long id) throws CategorieNotFoundException {
		categorieService.delete(id);
	}

	@GetMapping(produces = "application/json")
	public List<Categorie> get() {
		return categorieService.get();
	}

	@GetMapping(path = "{id}", produces = "application/json")
	public Categorie get(@PathVariable Long id) throws CategorieNotFoundException {
		return categorieService.get(id);
	}

}
