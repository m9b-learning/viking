package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entity.Client;
import com.viking.service.ClientService;
import com.viking.service.exceptions.ClientNotFoundException;

@RestController
@RequestMapping(path = "/client")
public class ClientController {

	@Autowired
	private ClientService clientService;

	@PostMapping()
    public ResponseEntity<Client> create(@RequestBody @Valid Client client, BindingResult itemBindingResult, UriComponentsBuilder builder) throws FieldErrorException {
		if (itemBindingResult.hasErrors()) { 
			throw new FieldErrorException(itemBindingResult);
        }
		
        clientService.create(client);
        URI uri = builder.pathSegment("client", String.valueOf(client.getId())).build().toUri();
        return ResponseEntity.created(uri).body(client);
    }
	
	@PutMapping(path = "{id}", produces = "application/json")
    public Client update(@PathVariable Long id, @RequestBody @Valid Client client, BindingResult itemBindingResult) throws ClientNotFoundException, FieldErrorException  {
		if (itemBindingResult.hasErrors()) { 
			throw new FieldErrorException(itemBindingResult);
        }
		
        client.setId(id);
        clientService.update(client);

        return client;
    }

	@DeleteMapping(path = "{id}", produces = "application/json")
	public void delete(@PathVariable Long id) throws ClientNotFoundException {
		clientService.delete(id);
	}

	@GetMapping(produces = "application/json")
	public List<Client> get() {
		return clientService.get();
	}

	@GetMapping(path = "{id}", produces = "application/json")
	public Client get(@PathVariable Long id) throws ClientNotFoundException {
		return clientService.get(id);
	}
}