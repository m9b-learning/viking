package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entity.Panier;
import com.viking.service.PanierService;
import com.viking.service.exceptions.ArticleNotFoundException;
import com.viking.service.exceptions.PanierNotFoundException;

@RestController
@RequestMapping(path = "/panier")
public class PanierController {

	@Autowired
	private PanierService panierService;

	@PostMapping()
    public ResponseEntity<Panier> create(@RequestBody Panier panier, UriComponentsBuilder builder) {
        panierService.create(panier);
        URI uri = builder.pathSegment("panier", String.valueOf(panier.getId())).build().toUri();
        return ResponseEntity.created(uri).body(panier);
    }
	
	@PostMapping(path = "{id}", produces = "application/json")
	public Panier update(@PathVariable Long id) throws PanierNotFoundException, ArticleNotFoundException {
		return panierService.validePanier(id);
	}
	
	// TODO : ajouter article au panier
	
	@DeleteMapping(path = "{id}", produces = "application/json")
	public void delete(@PathVariable Long id) throws PanierNotFoundException {
		panierService.delete(id);
	}

	@GetMapping(produces = "application/json")
	public List<Panier> get() {
		return panierService.get();
	}

	@GetMapping(path = "{id}", produces = "application/json")
	public Panier get(@PathVariable Long id) throws PanierNotFoundException {
		return panierService.get(id);
	}
	
	
}