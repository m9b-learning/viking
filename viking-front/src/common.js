$(document).ready(function () {

  $('#custom-file-input').on('change', function () {

    var fileName = $('#custom-file-input').val().split('\\').pop();
    $("#file-input-label").empty().append(fileName);

    readTmpFile(this);
  })

});



  function readTmpFile(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploaded_image').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }
