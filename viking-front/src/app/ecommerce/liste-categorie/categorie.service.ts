import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Categorie } from 'src/app/modeles/categorie';
import { map } from 'rxjs/operators';

@Injectable({
 providedIn: 'root'
})
export class CategorieService {
  baseUrl: string = "http://localhost:8080/viking-drakkar";
  constructor(private  http: HttpClient){}

   httpOptions = {
    headers: new HttpHeaders({
       'Content-type': 'application/json'
    })
  }; 
    //ici je peux changer mon get pour post pour envoyer les donnees
  getCategories(){
    return this.http.get<Categorie[]>(this.baseUrl+"/categorie", this.httpOptions).pipe(
      map(
        (jsonArray: Object[])=>jsonArray.map(jsonItem => Categorie.fromJson(jsonItem))
      ));  
    }
}