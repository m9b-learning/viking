import { Component, OnInit } from '@angular/core';
import { PanierService } from './panier.service';
import { Article } from 'src/app/modeles/article';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  panier: Article[];
  sub: Subscription;
  constructor(public panierService: PanierService) {

  }

  ngOnInit() {
    this.sub = this.panierService.panier.subscribe(data => {
      this.panier = data;
      console.log(this.panier);
    })

  }
  clearPanier() {
    console.log("effacement du panier");
    this.panierService.clearPanier();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  totalPanier() {
    return 321;
  }
}
