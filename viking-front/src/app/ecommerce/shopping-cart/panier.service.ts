import { Injectable } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { Article } from 'src/app/modeles/article';


@Injectable({
    providedIn: 'root'
})
export class PanierService {

    liste: Article[] = [];
    public panier = new ReplaySubject<Article[]>();


    setPanier(liste: Article[]) {
        this.panier.next(liste);
        console.log("dans set panier" + this.panier);
        for (let art of this.liste) {
            console.log("article du panier : " + art.id);
        }
    }

    getPanier() {
        console.log("dans get panier" + this.panier);
        return this.panier;
    }

    addArticle(article: Article) {
        console.log("article pushé :" + article.id)
        this.liste.push(article);
        console.log(this.liste);
        console.log("liste des article du subject");
        for (let art of this.liste) {
            console.log(art.id);
        }

        this.panier.next(this.liste);
        console.log(this.liste);
    }

    deleteArticle(id: number) {
        let i = 0;
        let numeroAsupprimer = 0;
        for (let article of this.liste) {

            if (article.id === id) {
                console.log("suppression de l'article du panier")
                numeroAsupprimer = i;
            }
            i++;
        }
        this.liste.splice(numeroAsupprimer, 1);


        this.panier.next(this.liste);
        console.log(this.liste);
    }

    clearPanier() {

        this.liste = [];
        this.panier.next(this.liste);
    }
}