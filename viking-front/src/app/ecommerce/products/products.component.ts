import { Component, OnInit,Input } from '@angular/core';
import { Article } from 'src/app/modeles/article';
import { ArticleService } from './article.service';
import { PanierService } from '../shopping-cart/panier.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  listeArticles: Article[];

  constructor(private articleService: ArticleService, private panierService: PanierService) { }

    ngOnInit() {
      this.articleService.getArticles().subscribe(data=> {
        this.listeArticles = data;
        this.selectedArticle = this.listeArticles[0];
      });

    }
    selectedArticle: Article;
    onSelect (art: Article): void {
      this.selectedArticle = art;
    }

    addArticle (art: Article): void {
      this.panierService.addArticle(art);
    }

}
