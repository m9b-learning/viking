import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Article } from 'src/app/modeles/article';
import { map } from 'rxjs/operators';
import { Categorie } from 'src/app/modeles/categorie';
import { Constructeur } from 'src/app/modeles/constructeur';

@Injectable({
 providedIn: 'root'
})
export class ArticleService {
  baseUrl: string = "http://localhost:8080/viking-drakkar";
  constructor(private  http: HttpClient){}

   httpOptions = {
    headers: new HttpHeaders({
       'Content-type': 'application/json'
    })
  };
    //ici je peux changer mon get pour post pour envoyer les donnees
  getArticles(){
    return this.http.get<Article[]>(this.baseUrl+"/article", this.httpOptions).pipe(
      map(
        (jsonArray: Object[])=>jsonArray.map(jsonItem => Article.fromJson(jsonItem))
      ));
    }

  createArticle() {

    let categorie = new Categorie();
    categorie.setId(1);
    categorie.setNom("écran");

    let constructeur = new Constructeur();
    constructeur.setId(1);
    constructeur.setNom("asus");

    let article = new Article();
    article.setDesignation("Article de test");
    article.setCategorie(categorie);
    article.setPrix(10);
    article.setDescription("Lorem ipsum");
    article.setConstructeur(constructeur);
    article.setStock(1);


    console.log("appel de createArticle");
    console.log(JSON.stringify(article));


    const req = this.http.post(this.baseUrl + "/article", JSON.stringify(article), { headers: { 'Content-type': 'application/json' } }).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured");
      }
    );
  }
}
