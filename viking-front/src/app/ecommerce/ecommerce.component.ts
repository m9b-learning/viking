import { Component, OnInit } from '@angular/core';
import { Article } from '../modeles/article';
import { Categorie } from 'src/app/modeles/categorie';
import { Constructeur } from 'src/app/modeles/constructeur';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ecommerce',
  templateUrl: './ecommerce.component.html',
  styleUrls: ['./ecommerce.component.css']
})
export class EcommerceComponent implements OnInit {
  baseUrl: string = "http://localhost:8080/viking-drakkar";
  model: any = {};

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  };


  ngOnInit() {
  }

  onSubmit() {
    // console.log(form.designation);
    console.log('########### ' + this.model.designation)
  }

  createArticle() {
    console.log('>>>>>>>>> ');
    console.log(this.model);
    console.log('>>>>>>>>> ');


    let categorie = new Categorie();
    categorie.setId(1);
    categorie.setNom("écran");

    let constructeur = new Constructeur();
    constructeur.setId(1);
    constructeur.setNom("asus");

    let article = new Article();
    article.setDesignation("Article de test");
    article.setCategorie(categorie);
    article.setPrix(10);
    article.setDescription("Lorem ipsum");
    article.setConstructeur(constructeur);
    article.setStock(1);


    console.log("appel de createArticle");
    console.log(JSON.stringify(article));

    const req = this.http.post(this.baseUrl + "/article", JSON.stringify(article), { headers: { 'Content-type': 'application/json' } })
    .subscribe(
      req => {
        console.log(req);
      },
      err => {
        console.log("Error occured");
      }
    );
  }
}
