import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../products/article.service';

@Component({
  selector: 'app-creation-article',
  templateUrl: './creation-article.component.html',
  styleUrls: ['./creation-article.component.css']
})
export class CreationArticleComponent implements OnInit {

  constructor(private articleService: ArticleService) {

   }

  ngOnInit() {
    this.articleService.createArticle;
  }
   
}
