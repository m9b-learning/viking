export class Constructeur {
    public id: number;
    public nom: string;

    public static fromJson(json: Object): Constructeur {

        let constructeur = new Constructeur;
        constructeur.setId(json['id']);
        constructeur.setNom(json['nom']);

        return constructeur;
    }

    getId() { return this.id };
    getNom() { return this.nom };

    setId(id: number) { this.id = id };
    setNom(nom: string) { this.nom = nom };
}