import { Categorie } from 'src/app/modeles/categorie';
import { Constructeur } from 'src/app/modeles/constructeur';

export class Article {

    public id: number;
    public designation: string;
    public categorie: Categorie;
    public image: string;
    public prix: number;
    public description: string;
    public constructeur: Constructeur;
    public dateMiseEnLigne: string;
    public stock: number;


    public static fromJson(json: Object): Article {

        let article = new Article();
        article.setId(json['id']);
        article.setDesignation(json['designation']);
        article.setCategorie(json['categorie']);
        article.setImage(json['image']);
        article.setPrix(json['prix']);
        article.setDescription(json['description']);
        article.setConstructeur(json['constructeur']);
        article.setDateMiseEnLigne(json['dateMiseEnLigne']);
        article.setStock(json['stock']);

        return article;
    }

    getId() { return this.id };
    getDesignation() { return this.designation };
    getCategorie() { return this.categorie };
    getImage() { return this.image };
    getPrix() { return this.prix };
    getDescription() { return this.description };
    getConstructeur() { return this.constructeur };
    getDateMiseEnLigne() { return this.dateMiseEnLigne };
    getStock() { return this.stock };

    setId(id: number) { this.id = id };
    setDesignation(designation: string) { this.designation = designation };
    setCategorie(categorie: Categorie) { this.categorie = categorie };
    setImage(image: string) { this.image = image };
    setPrix(prix: number) { this.prix = prix };
    setDescription(description: string) { this.description = description };
    setConstructeur(constructeur: Constructeur) { this.constructeur = constructeur };
    setDateMiseEnLigne(dateMiseEnLigne) { this.dateMiseEnLigne = dateMiseEnLigne };
    setStock(stock: number) { this.stock = stock };

}