export class Categorie {
    public id: number;
    public nom: string;

    public static fromJson(json: Object): Categorie {

        let categorie = new Categorie;
        categorie.setId(json['id']);
        categorie.setNom(json['nom']);

        return categorie;
    }

    getId() { return this.id };
    getNom() { return this.nom };

    setId(id: number) { this.id = id };
    setNom(nom: string) { this.nom = nom };
}